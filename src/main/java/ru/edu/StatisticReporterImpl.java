package ru.edu;




import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class StatisticReporterImpl implements StatisticReporter {
    /**
     *  Файл с результатом (отчетом).
     */
    private File file;

    /**
     * Конструктор с указанием пути для отчета.
     *
     * @param filePath
     */
    public StatisticReporterImpl(final String filePath) {
        file = new File(filePath);
    }

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {
        try (FileOutputStream os = new FileOutputStream(file);
             OutputStreamWriter sw = new OutputStreamWriter(os,
                     StandardCharsets.UTF_8);
             PrintWriter writer = new PrintWriter(sw)) {
            writer.println("Words: "
                    + statistics.getWordsCount());
            writer.println("Characters: "
                    + statistics.getCharsCount());
            writer.println("Without Spaces: "
                    + statistics.getCharsCountWithoutSpaces());
            writer.println("Punctuations: "
                    + statistics.getCharsCountOnlyPunctuations());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
